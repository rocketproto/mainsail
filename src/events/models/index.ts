export enum MainsailEvents {
    GAME_INIT = 0,
    PLAYER_JOIN = 1,
    PLAYER_SPAWNED = 2,
    PLAYER_KILLED = 3,
    PLAYER_KILL = 4,
    PLAYER_UPDATE = 5,
    CHAT = 6,
    PLAYER_LEFT = 7,
    GAME_END = 8,
}

export interface IGameInitEvent extends IEventMessage {
    mapSeed: number,
}

export interface IPlayerUpdateEvent extends IEventMessage {
    boaty: IBoaty
}

export interface IPlayerJoinEvent {

}

export interface IEvent {
    userHash: string,
    timestamp: number,
    type: MainsailEvents,
    message: IEventMessage
}

export interface IEventMessage { }

export interface IPlayer {
    userName: string,
    userHash: string,
    // colour: string,
}

export interface IBoaty {
    position: ICartesian
    heading: Radians
    speed: Speed
    direction: Direction
}

export interface ICartesian {
    x: number;
    y: number;
}

export enum Direction {
    left = 1,
    straight = 0,
    right = -1,
}

export enum Speed {
    stop = 0,
    low = 1,
    half = 2,
    full = 3,
}

export type Radians = number;