import { BehaviorSubject, Observable, Subscription, interval } from "rxjs";
import { IMainsailConfig } from "./services/models/config";
import { IEvent, IEventMessage, IGameInitEvent, IPlayer } from "./events/models";
import { LocalPlayerService } from "./services/local-player.service";
import { RemotePlayerService } from "./services/remote-player.service";
import { ChatService } from "./services/chat.service";
import { CannonballService } from "./services/cannonball.service";
import { MatchService } from "./services/match.service";

export class Mainsail {

    public ticker: Observable<number>;
    private unsentEvents: IEvent[] = [];
    private outboundEventBus = new BehaviorSubject<IEvent[]>([]);
    public outboundEventBus$ = this.outboundEventBus.asObservable();
    private _tickerSub: Subscription;
    private _localPlayerService: LocalPlayerService;
    // private _remotePlayerService: RemotePlayerService;
    // private _chatService: ChatService;
    // private _cannonBallService: CannonballService;
    // private _logService: LogService;
    // private _matchService: MatchService;


    constructor(
        private config: IMainsailConfig
    ) {
        this.ticker = interval(config.tickInterval ? config.tickInterval : 33)
        this._tickerSub = this.ticker.subscribe(() => {
            this.tickHandler();
        });

        this._localPlayerService = new LocalPlayerService();
        // this._remotePlayerService = new RemotePlayerService();
        // this._chatService = new ChatService();
        // this._cannonBallService = new CannonballService();
        // this._logService = new LogService();
        // this._matchService = new MatchService();
    }

    public handleIncomingEvent(event: IEvent): void {

    }

    public registerLocalPlayer(username: string): void {
        this._localPlayerService.login(username);
    }

    private gameInitialise(event: IGameInitEvent): void {

    }

    public destroy(): void {
        // CLEAN UP
    }

    private tickHandler(): void {
        const eventMessages: IEventMessage[] = [];
        console.log("Tick");
        eventMessages.push(this._localPlayerService.nextClockTick());

        const events: IEvent[] = [];

        eventMessages.forEach((message) => {
            // const 
        });


    }


}