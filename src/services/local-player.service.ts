import { BehaviorSubject } from "rxjs";
import { Direction, IBoaty, IEventMessage, IPlayer, IPlayerUpdateEvent, Speed } from "../events/models";
import { Boaty } from "../classes/boaty";

export class LocalPlayerService {

    public userName: string = '';
    public userHash: string = '';
    // public colour: string = '';
    public boaty: Boaty;
    public registered: boolean = false;
    private outboundEventBus = new BehaviorSubject<IEventMessage[]>([]);
    public outboundEventBus$ = this.outboundEventBus.asObservable();

    /**
     * Service for managing the Local player.
     * Manages inputs and calculation of movement.
     */
    constructor() {
        this.boaty = new Boaty();
    }

    public login(userName: string): IPlayer {

        if (userName === '' || userName === undefined) {
            throw new Error("user name must be set.");
        }

        this.userName = userName;
        this.userHash = `${userName}-${Date.now()}`;

        this.registered = true;

        return {
            userName: this.userName,
            userHash: this.userHash
        }

    }

    public logout(): void {
        this.registered = false;

        this.userHash = "";
        this.userName = "";
    }

    public nextClockTick(): IPlayerUpdateEvent {
        this.boaty.move();
        return { boaty: this.boaty }
    }

    public setDirection(direction: Direction): void {
        this.boaty.direction = direction;
    }

    public setSpeed(speed: Speed): void {
        this.boaty.speed = speed;
    }

    public fireCannon(): void {

    }
}