import { BehaviorSubject } from "rxjs";
import { IEventMessage } from "../events/models";

export class ChatService {


    private outboundEventBus = new BehaviorSubject<IEventMessage[]>([]);
    public outboundEventBus$ = this.outboundEventBus.asObservable();

    /**
     * Service for managing all in game chat messages.
     */
    constructor() { }
}