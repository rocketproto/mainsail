import { BehaviorSubject } from "rxjs";
import { IEventMessage } from "../events/models";



export class CannonballService {

    private outboundEventBus = new BehaviorSubject<IEventMessage[]>([]);
    public outboundEventBus$ = this.outboundEventBus.asObservable();

    /**
     * Service for managing all cannonballs and collision events.
     */
    constructor() { }
}