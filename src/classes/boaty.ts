import { Direction, IBoaty, ICartesian, Radians, Speed } from "../events/models";

const SCALAR = 0.05;

export class Boaty implements IBoaty {

    public position: ICartesian;
    public heading: Radians;
    public speed: Speed;
    public direction: Direction;

    constructor() {
        this.position = { x: 0, y: 0 };
        this.heading = 1.8;
        this.speed = 3;
        this.direction = 0;
    }

    public move(): void {
        this.heading += (this.direction * SCALAR); // INCLUDE TURN REDUCTION FOR FULL MAST
        this.position.x += this.speed * Math.sin(this.heading);
        this.position.y += this.speed * Math.cos(this.heading);
        console.log("Position", this.position);
    }

    public setPosition(position: ICartesian): void {
        this.position = position;
    }

    public setDirection(direction: Direction): void {
        this.direction = direction;
    }

    public setSpeed(speed: Speed): void {
        this.speed = speed;
    }
}