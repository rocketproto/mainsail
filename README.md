# Mainsail





# Drafting

## Requirements
- Independent of Angular
- RXJS driven
- Event Source Pattern
- Typescript
- Game handled externally

## Events

- Game init
- Player Joined
- player spawn
- player killed
- player X killed player Y
- Player update (Including position and cannonballs)
- Chat
- Player left
- Game over


## Services

### Event Service
The highest Level, managing all outgoing events

### RemotePlayerService
Handles all remote players and extrapolation between updates.

### LocalPlayerService
Handles local player and input

### CannonBallService
Handles ALL cannonball events, kills, etc.

### Chat Service
Handles in game chat room.

### GamesService
Handles everything game related such as score, timer, map etc